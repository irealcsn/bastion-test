# variable "test_var" {
#   type = string
#   default = "test?value"
# }

variable "security_groups" {
  type = list(string)
  default = ["sg-0415f6d3ff7acbaad"]
}

variable "subnet_id" {
  type = string
  default = "subnet-0ae5f2625084a467b"
}

variable "key_pair_name" {
  type = string
  default = "crealkey-ohio"
}

variable "name_region" {
  type = string
  default = "us-east-1"
  #default = "us-east-2" #OHIO
}

variable "id_vpc_ohio" {
  type = string
  default = "vpc-067de90b0bbb7e374"
}