# -- VPC --
# # module "UltraVPC" {
# #   source = "./modules/VPC"
# #   vpc_config = {
# #     cidr = "60.0.0.0/16",
# #     name = "ansible-vpc",
# #     region = var.name_region
# #   }
# #   subnets_publicas = {
# #     az_a = {
# #       name = "ansible-subnet",
# #       cidr_block = "60.0.0.0/24"
# #     }
# #   }
# #   unique_id = "6.5.22"
# #   vpc_tags = {}
# # }

# -- UBUNTU --
# # module "ubuntu" {
# #   source = "./modules/EC2U"
# #   bastion_u_config = {
# #     instance_class  = "t2.micro",
# #     key_pair_name   = var.key_pair_name,
# #     subnet_id       = module.UltraVPC.subnets_publicas[0],
# #     id_vpc_ohio     = module.UltraVPC.vpc_id,
# #     id_sg           = module.UltraVPC.id_sg
# #   }
# # }
# -- WINDOWS --
# # module "windows" {
# #   source = "./modules/EC2W"
# #   bastion_w_config = {
# #     instance_class  = "t3.medium",
# #     key_pair_name   = var.key_pair_name,
# #     security_groups = [module.UltraVPC.id_sg],
# #     subnet_id       = module.UltraVPC.subnets_publicas[0]
# #   }
# #   unique_id         = "A"
# #   vpc_tags          = {}
# # }


# *--- VIRGINA - ECR ---*
module "ubuntu" {
  source = "./modules/EC2Generic"
}