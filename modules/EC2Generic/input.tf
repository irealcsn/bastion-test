# -- ALL this default values was in Virginia ---
variable "bastion_u_config" {
  type = object({
    instance_class  = string,
    key_pair_name   = string,
    subnet_id       = string,
    id_sg           = string
  })

  default = {
    instance_class = "t2.micro"
    key_pair_name = "crealkey"
    subnet_id = "subnet-bc767f92"
    id_sg = "sg-095bd3a3dcbade337"
  }

}

variable "NameTag" {
  type = string
  default = "test-ecr-cli"
}