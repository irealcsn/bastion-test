# --- REGION for every OS and edit in resource ---

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
# --- END REGION ---
resource "aws_instance" "test_bastion_x" {
  count                   = 1

  ami                     = data.aws_ami.ubuntu.id
  instance_type           = var.bastion_u_config.instance_class
  vpc_security_group_ids  = [var.bastion_u_config.id_sg]
  subnet_id               = var.bastion_u_config.subnet_id
  key_name                = var.bastion_u_config.key_pair_name
  user_data               = templatefile("${path.module}/script.sh", {})

  tags = {
    Name = var.NameTag
  }
}