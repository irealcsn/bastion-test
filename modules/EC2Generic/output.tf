output "public_ip" {
  value = [for s in aws_instance.test_bastion_x: s.public_ip]
  # value = values(aws_instance.map)[*].public_ip
  # value = values(aws_instance.test_bastion_x.)
  # value = aws_instance.test_bastion_x.public_ip
}