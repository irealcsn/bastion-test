
cp ~/key/crealkey-ohio.pem ~/bastion-test/EC2U/

---
[webserver] 
10.4.1.141 ansible_user=ubuntu

ansible production --private-key crealkey-ohio.pem -m ping

ansible all -i hosts --private-key crealkey-ohio.pem -m ping


ansible-playbook -i hosts name.yaml --check

ansible-playbook -i hosts --private-key crealkey-ohio.pem nginx.yaml --check
ansible-playbook -l production -i hosts --private-key crealkey-ohio.pem nginx.yaml


NameFile
nginx-playbook.yaml
hosts
crealkey-ohio.pem
sudo chmod 600 crealkey-ohio.pem

flowMaster
1. copy and update hosts
2. copy key and give chmod
3. copy playbook
4. run playbook