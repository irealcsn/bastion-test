variable "bastion_u_config" {
  type = object({
    instance_class  = string,
    key_pair_name   = string,
    subnet_id       = string,
    id_vpc_ohio     = string,
    id_sg           = string
  })
}