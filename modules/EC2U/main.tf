# -- UBUNTU --
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "test_bastion_x" {
  count                   = 1

  ami                     = data.aws_ami.ubuntu.id
  instance_type           = var.bastion_u_config.instance_class
  vpc_security_group_ids  = [var.bastion_u_config.id_sg]
  subnet_id               = var.bastion_u_config.subnet_id
  key_name                = var.bastion_u_config.key_pair_name
  user_data               = count.index == 0 ? templatefile("${path.module}/scriptMaster.sh", {}) : templatefile("${path.module}/scriptWorker.sh", {})

  tags = {
    Name = count.index == 0 ? "Ansible_Master": "Ansible_Worker${count.index}"
  }
}