#!/bin/bash
sudo mkdir /home/ubuntu/Master
sudo echo "Bienvenido al nodo master" > /home/ubuntu/Master/master.txt

sudo apt-get update 
sudo apt install -y software-properties-common
sudo apt install -y python3
sudo apt install -y python3.8-venv
sudo python3 -m venv my_app/env
#sudo -s
#source ./my_app/env/bin/activate
#pip install pywinrm
#pip install ansible
#deactivate
#exit
# sudo apt-add-repository -y ppa:ansible/ansible
# sudo apt-get update 
# sudo apt-get install -y ansible

sudo ssh-keygen -q -t rsa -N '' <<< $'\ny' >/dev/null 2>&1
# sudo ssh-agent bash
# sudo ssh-add ~/.ssh/id_rsa
# sudo cp .ssh/id_rsa.pub authorized_keys
# sudo chmod 644 .ssh/authorized_keys
# sudo ssh-copy-id ubuntu@