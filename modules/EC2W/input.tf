
locals {
  chocoInstall = [
    "azure-data-studio",
    "googlechrome",
    "sql-server-management-studio",
    "postman",
    "vscode",
  ]
  
  paquetes = join(" ", local.chocoInstall)

}

variable "bastion_w_config" {
  type = object({
    instance_class  = string,
    key_pair_name   = string,
    security_groups = list(string),
    subnet_id       = string
  })
}

variable "unique_id" {
  type    = string
}

variable "vpc_tags" {
  type = map(any)
}