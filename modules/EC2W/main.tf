# -- WINDOWS --
data "aws_ami" "ws2019" {
  most_recent = true

  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-Base-*"]
  }

  filter {
    name   = "platform"
    values = ["windows"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["801119661308"] # Amazon
}

resource "aws_instance" "bastion_w" {
  count = 0

  ami                    = data.aws_ami.ws2019.id
  instance_type          = var.bastion_w_config.instance_class
  vpc_security_group_ids = var.bastion_w_config.security_groups
  subnet_id              = var.bastion_w_config.subnet_id
  key_name               = var.bastion_w_config.key_pair_name
  user_data = templatefile("${path.module}/scripts/user_data1.tpl", { idWorker = count.index })
  # user_data = templatefile("${path.module}/scripts/user_data.tpl", { Paquetes = local.paquetes })

  get_password_data      = false

  tags = merge({
    Name = "Ansible_Worker${count.index}"
  }, var.vpc_tags)

  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      tags
      #instance_type
    ]
  }
}