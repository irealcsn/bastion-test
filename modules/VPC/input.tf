variable "vpc_config" {
  type = object({
      cidr = string,
      name = string,
      region = string
  })
}

variable "subnets_publicas" {
  type = object({
    az_a = object({
      name       = string,
      cidr_block = string,
    })
  })
}

variable "unique_id" {
  type    = string
}

variable "vpc_tags" {
  type = map(any)
}