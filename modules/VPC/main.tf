resource "aws_vpc" "main" {
  cidr_block = var.vpc_config.cidr

  tags = merge({
      Name = "${var.vpc_config.name}-${var.unique_id}"
  }, var.vpc_tags)
}

# Subnets publicas
resource "aws_subnet" "subnet_publica_a" {

  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnets_publicas.az_a.cidr_block
  availability_zone       = "${var.vpc_config.region}a"
  map_public_ip_on_launch = true

  tags = merge({
    Name = "${var.subnets_publicas.az_a.name}-${var.unique_id}"
  }, var.vpc_tags)
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = merge({
    Name = "igw-${var.vpc_config.name}-${var.unique_id}"
  }, var.vpc_tags)
}

resource "aws_route_table" "rt_publica" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge({
    Name = "route-table-publica-${var.unique_id}"
  }, var.vpc_tags)
}

resource "aws_route_table_association" "rt_public_association_a" {
  subnet_id      = aws_subnet.subnet_publica_a.id
  route_table_id = aws_route_table.rt_publica.id
}


resource "aws_security_group" "ansible-sg" {
  name        = "ansible-sg"
  description = "allow inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "from my ip range"
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["147.219.191.0/24"]
  }
  ingress {
    description = "from my ip range"
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["147.219.191.0/24"]
  }
  ingress {
    description = "from my ip range"
    from_port   = "3389"
    to_port     = "3389"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["147.219.191.0/24"]
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    protocol    = "-1"
    to_port     = "0"
  }
  tags = merge({
      Name = "${var.unique_id}-sg"
  }, var.vpc_tags)
}