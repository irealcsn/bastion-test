output "vpc_id" {
  value = aws_vpc.main.id
}
output "subnets_publicas" {
  value = [aws_subnet.subnet_publica_a.id]
}
output "id_sg" {
  value = aws_security_group.ansible-sg.id
}