# output "VPCId" {
#   value = module.UltraVPC.vpc_id
# }

# -- UBUNTU --
# output "test_out" {
#   value = data.aws_ami.ws2019
# }

output "public_ip_ubuntu" {
  value = [for s in module.ubuntu.public_ip: "ssh -i 'crealkey-ohio.pem' ubuntu@${s}"]
  # value = "ssh -i 'crealkey-ohio.pem' ubuntu@${module.ubuntu.public_ip} \n ssh -i 'crealkey-ohio.pem' ubuntu@${module.ubuntu.public_ip[1]}"
}

# -- WINDOWS --

# output "id_bastionWindows" {
#   value = module.windows.id_Windows
# }

# output "packages_windows" {
#   value = module.windows.InstallPackages
# }